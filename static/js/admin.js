/**
 * copyTextToClipboard function courtecy of Dean Taylor on stackoverflow
 * https://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript 
 */
function copyTextToClipboard(text) {
    var textArea = document.createElement("textarea");

    // Make sure textarea is invisible
    textArea.style.position = 'fixed';
    textArea.style.top = 0;
    textArea.style.left = 0;
    textArea.style.width = '2em';
    textArea.style.height = '2em';
    textArea.style.padding = 0;
    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';
    textArea.style.background = 'transparent';
    textArea.value = text;
    document.body.appendChild(textArea);

    // Select and copy contents
    textArea.select();

    try {
        var successful = document.execCommand('copy');
    } catch (err) {
        console.log('Oops, unable to copy');
    }

    // Clean up
    document.body.removeChild(textArea);
}

// Toggle selection for all ideas
$('#admin-page #toggle-all-ideas').click(function(){
    if ($(this).is(':checked')){
        $('input[type=checkbox][name="ids[]"]').prop('checked', true);
    }else{
        $('input[type=checkbox][name="ids[]"]').prop('checked', false);
    }
});

// Set the toggle all checkbox to its proper state
$('#admin-page input[type=checkbox][name="ids[]"]').click(function(){
    $('#toggle-all-ideas').prop("indeterminate", false);
    if ($('input[type=checkbox][name="ids[]"]:not(:checked)').length == 0){
        $('#toggle-all-ideas').prop('checked', true);
    }else if ($('input[type=checkbox][name="ids[]"]:checked').length == 0){
        $('#toggle-all-ideas').prop('checked', false);
    }else{
        $('#toggle-all-ideas').prop("indeterminate", true);
    }
});


// Perform the TeXify action and show the result in a modal.
$('#admin-page #button-texify').click(function(evt){
    evt.preventDefault();

    var params = $('input[type=checkbox][name="ids[]"]:checked').serializeArray();
    if (params.length === 0)
        return;

    var $placeholder = $('<div class="modal">').appendTo(document.body);
    var $button = $(this);
    $button.text('Loading…');

    // Submit TeXify requestd
    $placeholder.load($button.prop('formAction') + '?' + $.param(params) + ' .modal-dialog', function() {
        $placeholder.modal('show');
    });

    // Copy TeXified ideas to clipboard
    $placeholder.on('click', '*[data-dismiss=modal]', function(evt) {
        evt.preventDefault();
        copyTextToClipboard($placeholder.find('#texified-ideas').text());
        $placeholder.modal('hide');
    });

    // Remove dialog from HTML when hidden
    $placeholder.on('hidden.bs.modal', function() {
        $button.text('TeXify selected');
        $placeholder.remove();
    });
});
