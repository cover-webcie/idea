<?php
require_once 'include/models/Model.class.php';

class Idea extends Model
{
    public function __construct($db) {
        parent::__construct($db, 'idea');
    }

    /**
     * Query ideas by list of ID's
     */
    public function get_by_ids(array $ids, array $order=[]) {
        if (count($ids) === 0)
            return [];

        return $this->get(['id__in' => $ids], $order);
    }
}
