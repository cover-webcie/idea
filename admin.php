<?php
require_once 'include/init.php';
require_once 'include/form.php';


/** Renders and processes some CRUD operations for the Idea Model */
class IdeaView extends ModelView
{
    protected $views = ['read', 'list', 'update'];

    /** Run the page, but only for admin (board) members. */
    public function run_page() {
        if (!cover_session_in_committee(ADMIN_COMMITTEE))
            throw new HttpException(403, 'You are not allowed to see this page');
        else
            return parent::run_page();
    }

    /** Run the list view and optionally filter out archived messages  */
    protected function run_list() {
        if (!isset($_GET['filter']) || $_GET['filter'] === 'recent')
            $ideas = $this->get_model()->get(['archived' => 0], ['-submitted']);
        else if (isset($_GET['filter']) && $_GET['filter'] === 'all')
            $ideas = $this->get_model()->get([], ['-submitted']);
        else
            throw new HttpException(400, 'Filter is unknown!');


        return $this->render_template($this->get_template(), ['ideas' => $ideas]);
    }

    /** Archive selected ideas  */
    protected function run_update() {
        if ($_SERVER['REQUEST_METHOD'] !== 'POST')
            throw new HttpException(405, 'Method Not Allowed');
        
        if (isset($_POST['ids']))
            foreach ($_POST['ids'] as $id)
                $this->get_model()->update_by_id($id, ['archived' => 1]);
        
        return $this->redirect($this->get_success_url());
    }
}

// Create and run subdomain view
$view = new IdeaView('idea', 'Idea', get_model('Idea'));
$view->run();
