<?php
require_once 'include/init.php';
require_once 'include/form.php';

class IdeaForm extends Form
{
    public function __construct() {
        $fields = [
            'idea' => new StringField ('Idea', false, ['maxlength' => 4096]),
            'anonymous' => new CheckBoxField('Anonymous', true),
        ];
        parent::__construct('idea', $fields);
    }

    /**
     * Validate idea form
     */
    public function validate() {
        if (empty($this->get_value('idea')) || empty(trim($this->get_value('idea'))))
            // Prevent form processing if idea is empty, and don't give error messages on empty ideas
            return false;

        return parent::validate();
    }
}


/** Renders and processes the idea form */
class SubmissionView extends FormView
{
    protected $model;

    public function __construct() {
        parent::__construct('idea', 'Submit Idea');
        $this->model = get_model('Idea');
        $this->base_template = 'templates/idea_form.phtml';
    }

    /** Creates and returns the idea form */
    protected function get_form() {
        /** Only logged in members should be able to see the page, redirect to login page if not logged in. */
        if(!cover_session_logged_in()){
			$this->redirect(cover_login_url(SERVER_NAME));
        }

        return new IdeaForm();
    }

    /** Renders response indicating whether the valid form was successfully processed (or not) */
    protected function form_valid($form) {
        try {
            $this->process_form_data($form->get_values());
            $context = ['status' =>  'success'];
        } catch (Exception $e) {
            $context = [
                'status' => 'error', 
                'message' => 'Something went wrong: ' . $e->getMessage()
            ];
        }
        return $this->render_template($this->get_template('form_processed'), $context);
    }

    /** Renders response if nothing has been submitted or the submission is invalid */
    protected function form_invalid($form) {
        if (empty($form->get_field('idea')->errors)) {
            return $this->render_template($this->get_template('form'));
        }

        $context = [
            'status' => 'error', 
            'message' => implode('<br> ', array_unique($form->get_field('idea')->errors))
        ];
        return $this->render_template($this->get_template('form_processed'), $context);
    }


    /** Processes the data of a valid form */
    protected function process_form_data($data) {
        // If anonymous is checked, store cover_id in database
        if(isset($data['anonymous']) && $data['anonymous'] == 1){
            $data['cover_id'] = null;
        } else {
            $data['cover_id'] = get_cover_session()->id;
        }
        unset($data['anonymous']);
        $this->model->create($data);

        // Notify board
        list($subject) = preg_split("/\\r\\n|\\r|\\n/", $_POST['idea'], 2);
        $success = send_mail(
            SENDER_EMAIL,
            DESTINATION_EMAIL,
            $data['idea'],
            '[idea] '. $subject,
            ['Reply-To: ' . ADMIN_EMAIL]
        );

        // Determine wether email has ben send succesfully
        if (!$success)
            throw new HttpException(500, 'Your idea has been stored in our database, but we failed to notify the board!');
    }
}

// Create and run domain request view
$view = new SubmissionView();
$view->run();
