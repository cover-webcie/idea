<?php
require_once 'include/init.php';
require_once 'include/form.php';


/** Renders ideas to LaTeX compatible output */
class TexifyView extends TemplateView
{
    /** Run page logic */
    public function run_page() {
        if (!cover_session_in_committee(ADMIN_COMMITTEE))
            throw new HttpException(403, 'You are not allowed to see this page');
        else if (!isset($_GET['ids']))
            $content = 'No ideas selected';
        else {
            $ideas = get_model('Idea')->get_by_ids($_GET['ids'], ['submitted']);
            $content = '';
            foreach ($ideas as $idea)
                $content .= $this->texify_idea($idea);
        }

        return $this->render_template($this->get_template(), ['content' => $content]);
    }

    /** Make a single idea LaTeX compatible */
    protected function texify_idea($idea) {
        // Escape special characters
        $cover_id = $idea['cover_id'];
        $idea = $this->latex_escape($idea['idea']);
        // Replace single newlines by double newlines
        $idea = preg_replace("/(\\r\\n|\\r|\\n)+/", "\n\n", $idea);
        // Beautify single quotes
        $idea = preg_replace("/'(.*?)'/s", "`$1'", $idea);
        // Beautify double quotes
        $idea = preg_replace("/\"(.*?)\"/s", "``$1''", $idea);

        if($cover_id){
            $member = cover_get_member($cover_id);
            $member_name = $member->voornaam;
            print_r($member);
            if($member->tussenvoegsel)
                $member_name .= ' ' . $member->tussenvoegsel;
            $member_name .= ' ' . $member->achternaam;
            $member_name = htmlspecialchars($member_name);
            $sender_info = sprintf('\textit{Submitted by \textbf{%s}}', $member_name);
        } else {
            $sender_info = '\textit{Submitted anonymously}';
        }

        return sprintf("\idea{\n%s\n\n%s\n}{\n\n}\n", $idea, $sender_info);
    }

    /** Escape LaTeX special characters */
    protected function latex_escape($string) {
        $map = array( 
            '#'=>'\#',
            '$'=>'\$',
            '%'=>'\%',
            '&'=>'\&',
            '~'=>'\~{}',
            '_'=>'\_',
            '^'=>'\^{}',
            '\\'=>'\textbackslash',
            '{'=>'\{',
            '}'=>'\}',
        );

        return preg_replace_callback(
            "/([\^\%~\\\\#\$%&_\{\}])/",
            function ($matches) use ($map){
                return $map[$matches[0]];
            }, 
            $string );
    }
}

// Create and run texify view
$view = new TexifyView('texify', 'TeXify');
$view->run();
