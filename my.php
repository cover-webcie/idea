<?php
require_once 'include/init.php';
require_once 'include/form.php';


/** Renders and processes some CRUD operations for the Idea Model */
class IdeaView extends ModelView
{
    protected $views = ['read', 'list'];

    /** Run the page, but only for admin (board) members. */
    public function run_page() {
		/** Only logged in members should be able to see the page, redirect to login page if not logged in. */
		if(!cover_session_logged_in()) {
			$this->redirect(cover_login_url(SERVER_NAME));
		}

		return parent::run_page();
    }

    /** Run the list view and optionally filter out archived messages  */
    protected function run_list() {

		$ideas = $this->get_model()->get(['cover_id' => get_cover_session()->id], ['-submitted']);

        return $this->render_template($this->get_template(), ['ideas' => $ideas]);
    }
}

// Create and run subdomain view
$view = new IdeaView('my', 'My Ideas', get_model('Idea'));
$view->run();
